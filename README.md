# azure_infra_lab



## Getting started

In this lab we will deploy simple infra using Terraform and Azure.

# requirements
- Azure CLI


https://learn.microsoft.com/en-us/cli/azure/install-azure-cli-windows?tabs=azure-cli

- Terraform 
https://github.com/tfutils/tfenv


# Steps

Login using az


```
az login
```

Then create main.tf and use terraform
```
terraform init
terraform plan
terraform apply -target=resource.<name>
```

For ssh conection 
```
sudo chmod 400 linuxkey.pem
sudo ssh -i linuxkey.pem linuxusr@20.16.8.179
```


For app deployment use:

```
git clone https://github.com/Nexpeque/cicdworkshop

cd cicdworkshop/
sudo apt install npm

curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
sudo apt-get install -y nodejs

npm install


npm start
```
